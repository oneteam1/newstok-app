import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React from "react";
import MapContainer from "../components/MapContainer";
import NewsCards from "../components/NewsCards";

import "./Home.css";

const Home: React.FC = () => {
  return (
    <IonPage>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Newstock</IonTitle>
          </IonToolbar>
        </IonHeader>
        <MapContainer />
        <NewsCards />
      </IonContent>
    </IonPage>
  );
};

export default Home;
