import {
  IonAlert,
  IonAvatar,
  IonButton,
  IonButtons,
  IonChip,
  IonCol,
  IonContent,
  IonFooter,
  IonGrid,
  IonHeader,
  IonIcon,
  IonImg,
  IonInput,
  IonItem,
  IonItemDivider,
  IonLabel,
  IonModal,
  IonPage,
  IonRow,
  IonText,
  IonTextarea,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React, { useEffect, useState } from "react";
import axios from "axios";
import "./MapContainer.css";
import { useRef } from "react";
import {
  attachOutline,
  cloudUploadOutline,
  refreshOutline,
  closeOutline,
} from "ionicons/icons";
import { IconButton } from "@material-ui/core";
import AttachFileIcon from "@material-ui/icons/AttachFile";
import CloseIcon from '@material-ui/icons/Close';

interface ContainerProps {}

const Upload: React.FC<ContainerProps> = () => {
  const [uploadDisabled, setUploadDisabled] = useState<boolean>(true);
  const [showAlert, setShowAlert] = useState(false);
  const [showModal, setShowModal] = useState(false);

  const titleRef = useRef<HTMLIonInputElement>(null);
  const subtitleRef = useRef<HTMLIonInputElement>(null);
  const descriptionRef = useRef<HTMLIonTextareaElement>(null);

  const resetInputs = () => {
    titleRef.current!.value = "";
    subtitleRef.current!.value = "";
    descriptionRef.current!.value = "";
    setUploadDisabled(true);
  };

  const uploadClk = () => {
    console.log(titleRef.current!.value);
    console.log(subtitleRef.current!.value);
    console.log(descriptionRef.current!.value);
    setShowAlert(true);
  };

  const handleUploadButton = () => {
    if (
      titleRef.current!.value! &&
      subtitleRef.current!.value! &&
      descriptionRef.current!.value!
    ) {
      setUploadDisabled(false);
    } else {
      setUploadDisabled(true);
    }
  };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Newstok</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonGrid>
          <IonRow>
            <IonItem>
              <IonLabel position="floating">Title</IonLabel>
              <IonInput
                ref={titleRef}
                onIonChange={handleUploadButton}
              ></IonInput>
            </IonItem>
          </IonRow>
          <IonRow>
            <IonItem>
              <IonLabel position="floating">Subtitle</IonLabel>
              <IonInput
                ref={subtitleRef}
                onIonChange={handleUploadButton}
              ></IonInput>
            </IonItem>
          </IonRow>
          <IonRow>
            <IonItem>
              <IonRow>
                <IonCol>
                  <IonLabel position="floating">Description</IonLabel>
                </IonCol>

                <IonCol className="text-align-right">
                  <IconButton onClick={() => setShowModal(true)}>
                    <AttachFileIcon />
                  </IconButton>
                </IonCol>
              </IonRow>
              <IonRow>
                <IonTextarea
                  ref={descriptionRef}
                  placeholder="Type or Attach file"
                  onIonChange={handleUploadButton}
                ></IonTextarea>
              </IonRow>
            </IonItem>
          </IonRow>
        </IonGrid>

        <IonGrid>
          <IonRow>
            <IonCol>
              <IonButton
                class="ion-margin"
                disabled={uploadDisabled}
                onClick={uploadClk}
              >
                <IonIcon
                  slot="start"
                  ios={cloudUploadOutline}
                  md={cloudUploadOutline}
                ></IonIcon>
                Upload
              </IonButton>
              <IonButton
                class="ion-margin"
                fill="outline"
                onClick={resetInputs}
              >
                <IonIcon
                  slot="start"
                  ios={refreshOutline}
                  md={refreshOutline}
                ></IonIcon>
                Reset
              </IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>

        <IonAlert
          isOpen={showAlert}
          onDidDismiss={() => {
            setShowAlert(false);
          }}
          message="News has been published successfully"
          buttons={["Close"]}
        />

        <IonModal isOpen={showModal}>
          <IonHeader>
            <IonToolbar>
              <IonGrid>
                <IonRow>
                  <IonCol>
                    <IonTitle>Upload News</IonTitle>
                  </IonCol>
                  <IonCol className="text-align-right">
                  <IconButton onClick={() => setShowModal(false)}>
                    <CloseIcon />
                  </IconButton>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonToolbar>
          </IonHeader>
          <IonContent>
            <div className="modal-container">
              <p>Drag and drop files to upload</p>
              <IonButton>Select Files</IonButton>
            </div>
          </IonContent>
        </IonModal>
      </IonContent>
    </IonPage>
  );
};

export default Upload;
