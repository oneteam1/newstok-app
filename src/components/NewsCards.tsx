import React, { useEffect, useState } from "react";
import axios from "axios";
import "./MapContainer.css";
import { IonGrid, IonRow, IonSlides } from "@ionic/react";

import NewsCard from "../components/NewsCard";

interface ContainerProps {}

const NewsCards: React.FC<ContainerProps> = () => {
  const [cards, setCards] = useState([{}]);

  useEffect(() => {
    axios
      .get("http://13.126.252.216:8091/newstok/news")
      .then((response) => {
        console.log(response);
        setCards(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <IonGrid>
      <IonRow>
        <IonSlides>
          {cards.map((card) => (
            <NewsCard {...card} />
          ))}
        </IonSlides>
      </IonRow>
    </IonGrid>
  );
};

export default NewsCards;
