import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCol,
  IonImg,
  IonSlide,
} from "@ionic/react";

import React from "react";
import "./MapContainer.css";

interface ContainerProps {}

const NewsCard: React.FC<ContainerProps> = (props: any) => {
  return (
    <IonSlide>
      <IonCol>
        <IonCard href="news-details">
          <IonImg src={props.mediaLink}></IonImg>
          <IonCardHeader>
            <IonCardTitle>{props.title}</IonCardTitle>
            <IonCardSubtitle>{props.subtitle}</IonCardSubtitle>
          </IonCardHeader>
          <IonCardContent>
            {props.description && props.description.substring(0, 100)}
          </IonCardContent>
        </IonCard>
      </IonCol>
    </IonSlide>
  );
};

export default NewsCard;
