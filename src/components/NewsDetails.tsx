import {
  IonAvatar,
  IonChip,
  IonContent,
  IonGrid,
  IonHeader,
  IonImg,
  IonItem,
  IonLabel,
  IonPage,
  IonRow,
  IonText,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React, { useEffect, useState } from "react";
import axios from "axios";
import "./MapContainer.css";

interface ContainerProps {}

const NewsDetails: React.FC<ContainerProps> = () => {
  const [newsCard, setNewsCard] = useState({
    id: null,
    type: null,
    title: null,
    subtitle: null,
    description: null,
    hasMedia: false,
    mediaLink: null,
    areaCode: null,
    city: null,
    created: null,
    userId: null,
  });

  const [user, setUser] = useState({
    id: null,
    name: "Sanjay Sharma",
    email: null,
    avatar:
      "https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y",
  });

  useEffect(() => {
    axios
      .get("http://13.126.252.216:8091/newstok/news/N1001")
      .then((response) => {
        console.log(response);
        setNewsCard({
          id: response.data.id,
          type: response.data.type,
          title: response.data.title,
          subtitle: response.data.subtitle,
          description: response.data.description,
          hasMedia: response.data.hasMedia,
          mediaLink: response.data.mediaLink,
          areaCode: response.data.areaCode,
          city: response.data.city,
          created: response.data.created,
          userId: response.data.userId,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Newstock</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Newstock</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonGrid>
          <IonRow>
            <IonText>{newsCard.title}</IonText>
          </IonRow>
          <IonRow>
            <IonLabel className="news-details">
              {newsCard.city}, {newsCard.created}
            </IonLabel>
          </IonRow>
          <IonRow>
            <IonChip outline>
              <IonAvatar>
                <img src={user.avatar} />
              </IonAvatar>
              <IonLabel>{user.name}</IonLabel>
            </IonChip>
          </IonRow>

          <IonRow>
            <IonText>{newsCard.subtitle}</IonText>
          </IonRow>

          <IonRow>
            <IonItem>
              <IonImg src={newsCard.mediaLink} />
            </IonItem>
          </IonRow>

          <IonRow>
            <IonText>{newsCard.description}</IonText>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default NewsDetails;
