import React, { useEffect, useRef } from 'react';
import PersonIcon from '@material-ui/icons/Person';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import CloudUploadOutlinedIcon from '@material-ui/icons/CloudUploadOutlined';

import { useHistory } from "react-router-dom";

import './MapContainer.css';
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center'
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));


interface ContainerProps { }

const MapContainer: React.FC<ContainerProps> = () => {

    const history = useHistory();

    const mapEle = useRef<HTMLDivElement>(null)!;
    const map = useRef<google.maps.Map>();
    const classes = useStyles();
    useEffect(() => {
        const elementX = mapEle ? mapEle.current : null;
        
        map.current = new google.maps.Map(elementX, {
            center: {
              lat: 20,
              lng: 20
            },
            zoom: 4,
            disableDefaultUI: true
        });

        google.maps.event.addListenerOnce(map.current, 'idle', () => {
          if (mapEle.current) {
            mapEle.current.classList.add('show-map');
          }
        });
    });

    const customizedHeader = () => {
      return (
        <Paper component="form" className={classes.root}>
          <IconButton className={classes.iconButton} aria-label="menu">
            <MenuIcon />
          </IconButton>
          <InputBase
            className={classes.input}
            placeholder="Search Area for News"
            inputProps={{ 'aria-label': 'Search Area for News' }}
          />
          <IconButton type="submit" className={classes.iconButton} aria-label="search">
            <SearchIcon />
          </IconButton>
          <Divider className={classes.divider} orientation="vertical" />
          <IconButton className={classes.iconButton} onClick={() => history.push("upload")}>
            <CloudUploadOutlinedIcon />
          </IconButton>
          <Divider className={classes.divider} orientation="vertical" />
          <IconButton color="primary" className={classes.iconButton} aria-label="directions">
            <PersonIcon />
          </IconButton>
        </Paper>
      );
    }

    return (
        <React.Fragment>
          {customizedHeader()}
          <div ref={mapEle} className="map-canvas"></div>
        </React.Fragment>
    );
};

export default MapContainer;
